# Url Shortener SPA

[![Netlify Status](https://api.netlify.com/api/v1/badges/b5c8df67-0be3-438b-87b9-2df3633502cb/deploy-status)](https://app.netlify.com/sites/vigilant-fermat-25bafb/deploys)

## What is this thing?

This is a super quick implementation of a URL shortener app that uses the Goldbelly URL shortener endpoint.

## Getting Started

1. Open your terminal. Navigate to where this project should live. (i.e. `cd ~/Projects`)
2. `git clone git@github.com:eroepken/url-shortener.git`
3. `cd url-shortener && npm i`
4. Create a `.env` file with the following information populated:

```bash
REACT_APP_API_KEY='[Enter API secret here]'
REACT_APP_API_URL='https://api.bely.me/'
```

5. `npm run start`
6. Your browser should open to the app to http://localhost:3000.

## Development Commands

- Tests: Run `npm run test` to run the whole test suite.
- Linting: Run `npm run lint` to check formatting across all files, per ESLint and Prettier settings.
- Formatting: Run `npm run fmt` to fix formatting across all files, per ESLint and Prettier settings.

**Note:** Lint staged runs on every commit, which also runs formatting and linting.

## The Objective

> Your task is to build a front end for a URL shortening web service. The deliverable is the source code, written in React, using whichever libraries, frameworks, tools, and development methodologies you choose.
>
> The requirements intentionally leave out many details. This is an opportunity for you to make decisions about the design of the service. What you leave out is just as important as what you include!
>
> ### Product Requirements:
>
> The front end should be a single page app that allows the user to:
>
> - Input any URL to get a shortened version of it
> - See a list of previously shortened URLs
> - Expire/delete any previous URLs
>   The user should optionally be able to provide a custom slug to be used as the path in the shortened URL.
>
> ### Project Requirements:
>
> - The project should include an automated test suite.
> - The project should include a README file with instructions for running the web service and its tests. You should also use the README to provide context on choices made during development.
> - The project should be packaged as a zip file or submitted via a hosted git platform (Github, Gitlab, etc).

## Why I Chose Create React App

I used this particular method because I honestly wanted the quickest possible start with everything I was used to using, including TypeScript and testing. I also wanted to use an existing UI kit so I didn't spend a ton of time fussing with styles, so I included the Blueprint UI kit. (I can promise I'm actually pretty good at and enjoy writing CSS, but the perfectionist in me would have spent all weekend on it.)

## Completed Tasks

- [x] Input any URL to get a shortened version of it
- [x] See a list of previously shortened URLs
- [x] Expire/delete any previous URLs
- [x] Automated test suite
- [x] A README file with instructions for running the web service and its tests
- [x] Submitted via Gitlab

### Extras

- [x] Show pipeline in Gitlab
- [x] Deploy to Netlify

## Explanation of Tasks I Didn't Get to

I also intended to keep track of slugs and URLs in local storage and/or memoization of data to limit the number of calls to the API, but I didn't get around to it. I also have to admit that I didn't write great tests.
