/**
 * Call the API.
 * Docs here: https://api.bely.me/docs
 */

export type Operation = 'list' | 'create' | 'read' | 'delete'

const Operations = {
  list: {
    method: 'GET',
    path: 'links'
  },
  create: {
    method: 'POST',
    path: 'links'
  },
  read: {
    method: 'GET',
    path: 'links/:slug'
  },
  delete: {
    method: 'DELETE',
    path: 'links/:slug'
  }
}

/**
 * Get the URL needed for the API call.
 * @param {Operation} operation - The name of the operation to complete.
 * @param {string} slug - The ID of the shortened URL to get, if the operation needs it.
 */
export const getUrl = (operation: Operation, slug: string = ''): string =>
  `${process.env.REACT_APP_API_URL}${Operations[operation].path.replace(
    ':slug',
    slug
  )}`

type APIOptions = {
  operation: Operation
  url?: string
  slug?: string
}

export type ResponseUrl = {
  short_url: string
  url: string
  slug: string
  errors: {
    url: string[]
    slug: string[]
  }
}

/**
 * Fetch data from the API endpoint.
 * @param {APIOperation} operation - The operation to perform.
 * @param {CallableFunction} callback - The function to trigger when the API call is done. Make
 * sure this function can receive a single parameter of type JSON!
 */
export const callAPI = async (
  { operation, url, slug = undefined }: APIOptions,
  callback: CallableFunction
): Promise<void> => {
  const requestHeaders: HeadersInit = new Headers()
  requestHeaders.set('GB-Access-Token', process.env.REACT_APP_API_KEY || '')

  const params: RequestInit = {
    method: Operations[operation].method,
    headers: requestHeaders
  }

  const data = { url }

  if (slug) {
    Object.assign(data, { slug })
  }

  if (operation === 'create') {
    requestHeaders.set('Content-Type', 'application/json')
    Object.assign(params, { body: JSON.stringify(data) })
  }

  try {
    const response = await fetch(getUrl(operation, slug), params)
    if (operation === 'delete') {
      callback(response.status)
    } else {
      const json = await response.json()
      callback(json)
    }
  } catch (error) {
    console.error('Error:', error)
  }
}
