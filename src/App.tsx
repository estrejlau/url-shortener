import React, { useState } from 'react'
import { H1, Divider, Callout, Intent } from '@blueprintjs/core'

import { ResponseUrl } from './utils/callAPI'

import ShortenerForm from './components/ShortenerForm'
import ExistingLinks from './components/ExistingLinks'

import './App.css'

const App = () => {
  const [urlList, setUrlList] = useState([] as ResponseUrl[])

  return (
    <main className="container">
      <H1>URL Shortener</H1>
      <Callout
        className="info-callout"
        icon="info-sign"
        intent={Intent.PRIMARY}
      >
        Use this form to shorten a URL using the provided API. If you prefer to
        use a custom slug, you may provide one. If not, one will be provided
        from the API.
      </Callout>
      <ShortenerForm urlList={urlList} setUrlList={setUrlList} />
      <Divider className="new-section" />
      <ExistingLinks urlList={urlList} setUrlList={setUrlList} />
    </main>
  )
}

export default App
