import React, { FC, useEffect } from 'react'
import {
  H2,
  Button,
  NonIdealState,
  Icon,
  Toaster,
  Intent
} from '@blueprintjs/core'

import { ResponseUrl, callAPI } from '../../utils/callAPI'

import './styles.css'

interface IExistingLinksProps {
  urlList: ResponseUrl[]
  setUrlList: CallableFunction
}

const ExistingLinks: FC<IExistingLinksProps> = ({ urlList, setUrlList }) => {
  useEffect(() => {
    callAPI(
      {
        operation: 'list'
      },
      (response: ResponseUrl[]) => {
        setUrlList(response)
      }
    )
  }, [setUrlList])

  const deleteUrl = (slug: string, key: number) => {
    // eslint-disable-next-line no-restricted-globals
    const doubleCheck = confirm(
      'Are you sure you want to delete this shortcut?'
    )
    if (doubleCheck) {
      callAPI(
        {
          operation: 'delete',
          slug
        },
        (status: number) => {
          if (status === 204) {
            setUrlList(urlList.filter((v, k) => k !== key))
            Toaster.create().show({
              message: 'URL removed.',
              icon: 'tick',
              intent: Intent.SUCCESS
            })
          }
        }
      )
    }
  }

  return (
    <>
      <H2>Existing Links</H2>
      {urlList.length > 0 ? (
        <table className="bp3-html-table bp3-html-table-striped existing-links-table">
          <thead>
            <tr>
              <th>Short URL</th>
              <th></th>
              <th>Long URL</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {urlList.map((v, k) => (
              <tr id={`existing-link-${k}`} key={`existing-link-${k}`}>
                <td>
                  <a href={v.short_url} target="_new">
                    {v.short_url}
                  </a>
                </td>
                <td>
                  <Icon icon="arrow-right" />
                </td>
                <td>
                  <a href={v.url} target="_new">
                    {v.url}
                  </a>
                </td>
                <td>
                  <Button
                    icon="delete"
                    onClick={() => deleteUrl(v.slug, k)}
                    title={`Delete shortcut to ${v.url}`}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      ) : (
        <NonIdealState
          icon="link"
          title="No Links"
          description="Use the form above to add a new link."
        />
      )}
    </>
  )
}

export default ExistingLinks
