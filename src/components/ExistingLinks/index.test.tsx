import React from 'react'
import { render, screen } from '@testing-library/react'
import ExistingLinks from '.'

test('renders title text', () => {
  render(<ExistingLinks urlList={[]} setUrlList={() => {}} />)
  const titleElement = screen.getByText(/Existing Links/i)
  expect(titleElement).toBeInTheDocument()
})
