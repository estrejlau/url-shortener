import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import ShortenerForm from '.'

test('renders form', () => {
  const { getByRole } = render(
    <ShortenerForm urlList={[]} setUrlList={() => {}} />
  )
  expect(getByRole('button')).not.toHaveAttribute('disabled')
})

test('url is required', () => {
  const { getByLabelText } = render(
    <ShortenerForm urlList={[]} setUrlList={() => {}} />
  )
  expect(getByLabelText('URL (required)')).toBeRequired()
})

test('invalid urls are disallowed', () => {
  const label = 'URL (required)'
  const { getByLabelText } = render(
    <ShortenerForm urlList={[]} setUrlList={() => {}} />
  )
  fireEvent.change(getByLabelText(label), {
    target: { value: '23' }
  })
  fireEvent.submit(document.querySelector('form'))
  expect(getByLabelText(label)).toBeInvalid()
})

test('valid urls are allowed', () => {
  const label = 'URL (required)'
  const { getByLabelText } = render(
    <ShortenerForm urlList={[]} setUrlList={() => {}} />
  )
  fireEvent.change(getByLabelText(label), {
    target: { value: 'https://www.google.com' }
  })
  fireEvent.submit(document.querySelector('form'))
  expect(getByLabelText(label)).toBeValid()
})

test('slug is optional', () => {
  const { getByLabelText } = render(
    <ShortenerForm urlList={[]} setUrlList={() => {}} />
  )
  expect(getByLabelText('URL (required)')).toBeRequired()
})

test('slug must be at least 3 characters in length', () => {
  const label = 'Slug (optional)'
  const { getByLabelText } = render(
    <ShortenerForm urlList={[]} setUrlList={() => {}} />
  )
  fireEvent.change(getByLabelText(label), {
    target: { value: 'ab' }
  })
  fireEvent.submit(document.querySelector('form'))
  expect(getByLabelText(label)).toBeInvalid()
})

test('slug must only be alphanumeric', () => {
  const label = 'Slug (optional)'
  const { getByLabelText } = render(
    <ShortenerForm urlList={[]} setUrlList={() => {}} />
  )
  fireEvent.change(getByLabelText(label), {
    target: { value: '!@#$%' }
  })
  fireEvent.submit(document.querySelector('form'))
  expect(getByLabelText(label)).toBeInvalid()
})

test('valid slugs are allowed', () => {
  const label = 'Slug (optional)'
  const { getByLabelText } = render(
    <ShortenerForm urlList={[]} setUrlList={() => {}} />
  )
  fireEvent.change(getByLabelText(label), {
    target: { value: 'hello' }
  })
  fireEvent.submit(document.querySelector('form'))
  expect(getByLabelText(label)).toBeValid()
})

// test('slugs calculate prices properly', () => {
//   const label = 'Slug (optional)'
//   const { getByLabelText } = render(
//     <ShortenerForm urlList={[]} setUrlList={() => {}} />
//   )
//   fireEvent.change(getByLabelText(label), {
//     target: { value: 'hello' }
//   })
//   fireEvent.submit(document.querySelector('form'))
//   expect(getByLabelText(label)).toBeValid()
// })
