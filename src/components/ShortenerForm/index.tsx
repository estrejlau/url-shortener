import React, { FC, useState, FormEvent, useEffect } from 'react'
import {
  FormGroup,
  InputGroup,
  Button,
  Toaster,
  Intent,
  Tag
} from '@blueprintjs/core'

import { ResponseUrl, callAPI } from '../../utils/callAPI'

interface IShortenerFormProps {
  urlList: ResponseUrl[]
  setUrlList: CallableFunction
}

const ShortenerForm: FC<IShortenerFormProps> = ({ urlList, setUrlList }) => {
  const [url, setUrl] = useState('')
  const [slug, setSlug] = useState('')

  useEffect(() => {
    const timer = setTimeout(() => calculatePrice(slug), 1000)

    return () => {
      clearTimeout(timer)
    }
  }, [slug])

  const showErrorToaster = (messages: string | string[]) => {
    Toaster.create().show({
      message: messages,
      icon: 'error',
      intent: Intent.DANGER
    })
  }

  /**
   * - The cost of a slug will be calculated as:
   * - Non-vowel: $1
   * - Vowels: $2
   * - Repeated letters: $1 extra per appearance (apart from the first time).
   * @param slug
   */
  const calculatePrice = (slug: string) => {
    const vowels = ['a', 'e', 'i', 'o', 'u']

    let cost = 0

    const slugArray = slug.split('')

    const letters: string[] = []

    slugArray.forEach((v) => {
      const exists = letters.includes(v)
      const isVowel = vowels.includes(v)

      if (exists) {
        cost = cost + (isVowel ? 3 : 2)
      } else {
        letters.push(v)
        cost = cost + (isVowel ? 2 : 1)
      }
    })

    return cost
  }

  const shortenUrl = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()

    const exists = urlList.filter((v) => v.url === url)
    if (exists.length > 0) {
      showErrorToaster(
        `URL is already being redirected from ${exists[0].short_url}.`
      )
      return
    }

    // Trigger the API call here if the URL is not empty.
    callAPI(
      {
        operation: 'create',
        url,
        slug
      },
      (response: ResponseUrl) => {
        if (!response.errors) {
          setUrlList([...urlList, response])
          Toaster.create().show({
            message: 'New URL Added!',
            icon: 'tick',
            intent: Intent.SUCCESS
          })
        } else {
          const messages = []

          if (response.errors.url) {
            messages.push(...response.errors.url.map((v) => `URL ${v}.`))
          }
          if (response.errors.slug) {
            messages.push(...response.errors.slug.map((v) => `Slug ${v}.`))
          }

          showErrorToaster(messages)
        }
      }
    )
  }

  return (
    <form onSubmit={shortenUrl}>
      <FormGroup
        helperText="The URL to which the short URL should redirect."
        label="URL"
        labelFor="url"
        labelInfo="(required)"
      >
        <InputGroup
          type="url"
          id="url"
          asyncControl={true}
          large={true}
          required
          leftIcon="link"
          onInvalid={(e) => {
            e.preventDefault()
            e.currentTarget.classList.add('bp3-intent-danger')

            showErrorToaster(
              e.currentTarget.value
                ? 'Please enter a valid URL.'
                : 'The URL cannot be empty.'
            )
          }}
          onChange={(e) => {
            e.currentTarget.classList.remove('bp3-intent-danger')
            setUrl(e.currentTarget.value)
          }}
        />
      </FormGroup>

      <FormGroup
        helperText="The custom path that the short URL should have."
        label="Slug"
        labelFor="slug"
        labelInfo="(optional)"
      >
        <InputGroup
          minLength={3}
          pattern="([A-z0-9]){3,}"
          id="slug"
          asyncControl={true}
          large={true}
          leftIcon="path"
          rightElement={<Tag minimal={true}>${calculatePrice(slug)}</Tag>}
          onInvalid={(e) => {
            e.preventDefault()
            e.currentTarget.classList.add('bp3-intent-danger')

            showErrorToaster(
              e.currentTarget.value.length >= 3
                ? 'Please enter only alphanumeric characters for the slug.'
                : 'The slug must be at least 3 characters in length.'
            )
          }}
          onChange={(e) => {
            e.currentTarget.classList.remove('bp3-intent-danger')
            setSlug(e.currentTarget.value)
          }}
        />
      </FormGroup>
      <Button type="submit" large={true}>
        Shorten URL
      </Button>
    </form>
  )
}

export default ShortenerForm
